# Spario
is an awesome responsive HTML5 website starting from scratch. It includes as many different features as possible, so we will be dealing with a jQuery slider, CSS3 transitions and animations, CSS Media Queries and more.

Following this [Tutorial](https://www.lingulo.com/tutorials/css/how-to-build-a-html5-website-from-scratch)

